let provincia;
let ciudad;
let fecha;
let hora;
let asiento;
let nombre;
let dni;
let edad;
let email;
let precio;

let obtenerDatosURL = () => {
    let arr = location.href.split('?');
    if (arr.length > 1 && arr[1] !== '') {
        let params = new URLSearchParams(location.search);
        provincia = params.get("provincia-reserva").replace(/-/g, ' ').toUpperCase();
        ciudad = params.get("ciudad-reserva").replace(/-/g, ' ').toUpperCase();
        fecha = params.get("fecha-reserva");
        hora = params.get("hora-reserva");
        asiento = params.get("asiento-reserva");
        nombre = params.get("nombre");
        dni = params.get("dni");
        edad = params.get("edad")
        email = params.get("email-reserva");
        precio = params.get("precio-reserva");
    } else {
        provincia = "Buenos Aires";
        ciudad = "Bahía Blanca";
        let rightNow = new Date();
        fecha = rightNow.toISOString().slice(0, 10);
        hora = "8:00";
        asiento = "A-1";
        nombre = "Juan Pérez";
        dni = "12345678";
        edad = "18";
        email = "juanperez@gmail.com";
        precio = "$10000";
    }
}

let completarDatos = () => {
    let provinciaDestino = document.getElementById("provincia");
    let ciudadDestino = document.getElementById("ciudad");
    let fechaReserva = document.getElementById("fecha");
    let horaReserva = document.getElementById("hora");
    let asientoReserva = document.getElementById("asiento");
    let nombrePasajero = document.getElementById("nombre");
    let dniPasajero = document.getElementById("dni");
    let edadPasajero = document.getElementById("edad");
    let emailPasajero = document.getElementById("email");
    let precioReserva = document.getElementById("precio");
    let nroReserva = document.getElementById("nro-reserva");
    provinciaDestino.innerHTML += " " + provincia;
    ciudadDestino.innerHTML += " " + ciudad;
    fechaReserva.innerHTML += " " + fecha;
    horaReserva.innerHTML += " " + hora;
    asientoReserva.innerHTML += " " + asiento;
    nroReserva.innerHTML += " " + "43377"
    nombrePasajero.innerHTML += " " + nombre;
    dniPasajero.innerHTML += " " + dni;
    edadPasajero.innerHTML += " " + edad;
    emailPasajero.innerHTML += " " + email;
    precioReserva.innerHTML += " " + precio;
}

obtenerDatosURL();
completarDatos();