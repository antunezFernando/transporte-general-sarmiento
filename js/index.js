let provincia = document.getElementById("provincias");
let ciudad = document.getElementById("ciudades");
let horarios = document.getElementById("horarios");

let calendario = document.getElementById("calendario");
let fechaActual = new Date();
let fechaEnSeisMeses = new Date();
fechaEnSeisMeses.setDate(fechaActual.getDate() + 182);
calendario.value = fechaActual.toISOString().split("T")[0];
calendario.min = fechaActual.toISOString().split("T")[0];
calendario.max = fechaEnSeisMeses.toISOString().split("T")[0];

let botonIngreso = document.getElementById("btn-ingreso");

let provincias = [
    {
        nombre: "Buenos Aires",
        ciudades: [
            "La Matanza",
            "Mar Del Plata",
            "Moreno",
            "San Miguel"
        ]
    },
    {
        nombre: "CABA",
        ciudades: [
            "Ciudad Autonoma de Buenos Aires"
        ]
    },
    {
        nombre: "Catamarca",
        ciudades: [
            "El Alto",
            "San Fernando Del Valle"
        ]
    },
    {
        nombre: "Chaco",
        ciudades: [
            "Resistencia"
        ]
    },
    {
        nombre: "Chubut",
        ciudades: [
            "Comodoro Rivadavia",
            "Rawson"
        ]
    },
    {
        nombre: "Cordoba",
        ciudades: [
            "Carlos Paz",
            "Cosquin"
        ]
    },
    {
        nombre: "Corrientes",
        ciudades: [
            "Goya",
            "Yapeyu"
        ]
    },
    {
        nombre: "Entre Rios",
        ciudades: [
            "Gualeguay",
            "Gualguaychu"
        ]
    },
    {
        nombre: "Formosa",
        ciudades: [
            "Clorinda",
            "Laguna Blanca"
        ]
    },
    {
        nombre: "Jujuy",
        ciudades: [
            "Purmamarca"
        ]
    },
    {
        nombre: "La Pampa",
        ciudades: [
            "Santa Rosa",
            "Toay"
        ]
    },
    {
        nombre: "La Rioja",
        ciudades: [
            "Famatina",
            "La Rioja"
        ]
    },
    {
        nombre: "Mendoza",
        ciudades: [
            "Godoy Cruz",
            "Mendoza"
        ]
    },
    {
        nombre: "Misiones",
        ciudades: [
            "Posadas",
            "Puerto Iguazu"
        ]
    },
    {
        nombre: "Neuquen",
        ciudades: [
            "Neuquen",
            "Zapala"
        ]
    },
    {
        nombre: "Rio Negro",
        ciudades: [
            "Bariloche",
            "Viedma"
        ]
    },
    {
        nombre: "Salta",
        ciudades: [
            "Purmamarca",
            "Salta"
        ]
    },
    {
        nombre: "San Juan",
        ciudades: [
            "Jachal",
            "San Juan"
        ]
    },
    {
        nombre: "San Luis",
        ciudades: [
            "Merlo"
        ]
    },
    {
        nombre: "Santa Cruz",
        ciudades: [
            "Perito Moreno",
            "Rio Gallegos"
        ]
    },
    {
        nombre: "Santa Fe",
        ciudades: [
            "Rosario",
            "Santo Tome"
        ]
    },
    {
        nombre: "Santiago del Estero",
        ciudades: [
            "Santiago del Estero"
        ]
    },
    {
        nombre: "Tierra del Fuego",
        ciudades: [
            "Rio Grande",
            "Ushuaia"
        ]
    },
    {
        nombre: "Tucuman",
        ciudades: [
            "San Miguel de Tucuman"
        ]
    }
]

let agregarProvincias = () => {
    let text = "<option value='seleccionar-provincia'>Seleccione una provincia</option>";
    for (let i = 0; i < provincias.length; i++) {
        let val = provincias[i].nombre.replace(/[\s+-]/g, '-').toLowerCase();
        text += "<option value='" + val + "'>" + provincias[i].nombre + "</option>";
    }
    provincia.innerHTML += text;
};

let agregarCiudades = () => {
    ciudad.innerHTML = "";
    let text = "<option value='seleccionar-ciudad'>Seleccione una ciudad</option>";
    for (let i = 0; i < provincias.length; i++) {
        if(provincia.value === provincias[i].nombre.replace(/[\s+-]/g, '-').toLowerCase()) {
            for (let j = 0; j < provincias[i].ciudades.length; j++) {
                let val = provincias[i].ciudades[j].replace(/[\s+-]/g, '-').toLowerCase();
                text += "<option value='" + val + "'>" + provincias[i].ciudades[j] + "</option>";
            }
        }
    }
    ciudad.innerHTML += text;
}

let agregarHorarios = () => {
    let text = "";

    for(let i = 6; i < 24; i++) {
        text += "<option value='"+ i + ":00'>" + i +":00</option>"
    }

    horarios.innerHTML += text;
}

let manejarBoton = () => {
    if(provincia.value === "seleccionar-provincia" || ciudad.value === "seleccionar-ciudad") {
        botonIngreso.disabled = true;
    }
    else {
        botonIngreso.disabled = false;
    }
}

agregarProvincias();
agregarCiudades();
agregarHorarios();
manejarBoton();