let provincia;
let ciudad;
let fecha;
let hora;
let email;

let obtenerDatosURL = () => {
    let arr = location.href.split('?');
    if (arr.length > 1 && arr[1] !== '') {
        let params = new URLSearchParams(location.search);
        provincia = params.get("provincias").replace(/-/g, ' ').toUpperCase();
        ciudad = params.get("ciudades").replace(/-/g, ' ').toUpperCase();
        fecha = params.get("calendario");
        hora = params.get("horarios");
        email = params.get("email")
    } else {
        provincia = "Buenos Aires";
        ciudad = "Bahía Blanca";
        let rightNow = new Date();
        fecha = rightNow.toISOString().slice(0, 10);
        hora = "8:00";
        email = "juanperez@gmail.com";
    }
}

let datosReserva = () => {
    let provinciaReserva = document.getElementById("provincia-reserva");
    let ciudadReserva = document.getElementById("ciudad-reserva");
    let fechaReserva = document.getElementById("fecha-reserva");
    let horaReserva = document.getElementById("hora-reserva");
    let emailReserva = document.getElementById("email-reserva");
    provinciaReserva.value = provincia;
    ciudadReserva.value = ciudad;
    fechaReserva.value = fecha;
    horaReserva.value = hora;
    emailReserva.value = email;
}

let seleccionarAsiento = (element) => {
    let asientos = document.getElementsByClassName("asiento");
    let asientoInput = document.getElementById("asiento-reserva");
    for(let i = 0; i < asientos.length; i++) {
        asientos[i].classList.remove("activo");
    }
    element.classList.add("activo");
    asientoInput.value = element.innerHTML;

    let precio = document.getElementById("precio-reserva")
    if(element.classList.contains("comun")) {
        precio.value = "$10000";
    } else {
        precio.value = "$17000";
    }
}

let crearAsientos = () => {
    let asientosIzq = document.getElementById("asientos-izq");
    asientosIzq.innerHTML += "<p>Arriba-izquierda</p>";
    let asientosDer = document.getElementById("asientos-der");
    asientosDer.innerHTML += "<p>Arriba-derecha</p>";
    let asientosEje = document.getElementById("asientos-eje");
    asientosEje.innerHTML += "<p>Abajo-izquierda</p>";
    for(let i = 0; i < 5; i++) {
        asientosIzq.innerHTML += "<div>";
        asientosDer.innerHTML += "<div>";
        asientosEje.innerHTML += "<div>";
        for(let j = 0; j < 2; j++) {
            asientosIzq.innerHTML += "<a class='asiento comun' onclick='seleccionarAsiento(this)'>" + String.fromCharCode(65 + i) + "-" + String.fromCharCode(49 + j) + "</a>";
            asientosDer.innerHTML += "<a class='asiento comun' onclick='seleccionarAsiento(this)'>" + String.fromCharCode(65 + i) + "-" + String.fromCharCode(51 + j) + "</a>";
            asientosEje.innerHTML += "<a class='asiento ejecutivo' onclick='seleccionarAsiento(this)'>" + String.fromCharCode(49 + i) + "-" + String.fromCharCode(51 + j) + "</a>";
        }
        asientosIzq.innerHTML += "</div>";
        asientosDer.innerHTML += "</div>";
        asientosEje.innerHTML += "</div>";
    }
}

obtenerDatosURL();
datosReserva();
crearAsientos();